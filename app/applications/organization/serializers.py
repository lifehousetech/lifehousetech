from rest_framework import serializers
from .models import Organization

class ListOrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = (
            'id',
            'name',
            'phone',
            'address',
        )