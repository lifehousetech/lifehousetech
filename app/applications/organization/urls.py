from django.urls import path
from rest_framework.routers import DefaultRouter,SimpleRouter
from rest_framework_nested import routers
from . import views

router = SimpleRouter()
router.register(r'users', views.OrganizationUserViewSet, basename='target_id')

urlpatterns = [
    path('api/organizations/<pk>/', views.ListOrganizationApiView.as_view()),
    # path('api/organizations/<pk>/user/', views.RetrieveUserOrgAPIView.as_view()),
    path('api/organizations/<int:id>/user/<int:target_id>/', views.OrganizationUserViewSet.as_view({"get": "users"}))
]