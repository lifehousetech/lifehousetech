from django.shortcuts import render
from rest_framework.generics import GenericAPIView,RetrieveAPIView,RetrieveUpdateDestroyAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from applications.organization.serializers import ListOrganizationSerializer
from rest_framework.decorators import action
from rest_framework.exceptions import NotAcceptable
from rest_framework.response import Response
from .models import Organization
from rest_framework.permissions import IsAdminUser
from rest_framework import filters

# Create your views here.

"""ORGANIZATION ENDPOINTS"""
class ListOrganizationApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = ListOrganizationSerializer
    queryset = Organization.objects.all()

    def get(self,request,pk):
        return self.retrieve(request,pk)
    
    def patch(self,request,pk):
        return self.partial_update(request,pk)
    # permission_classes = [Administrator,Viewer]

class RetrieveUserOrgAPIView(RetrieveAPIView):
    serializer_class = ListOrganizationSerializer
    queryset = Organization.objects.filter()

class OrganizationUserViewSet(ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = ListOrganizationSerializer

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(pk=self.request.user.organization_id)
        self.check_object_permissions(self.request, obj)
        return obj

    @action(methods=['get'], detail=True)
    def users(self, request, *args, **kwargs):
        user = self.get_object()
        target_id = int(kwargs['organization_id'])
        Organization.objects.get(name=user, target=target_id)
        return Response(status=status.HTTP_204_NO_CONTENT)
