from __future__ import unicode_literals
from django.db import models

# Create your models here.
class Organization(models.Model):
    name = models.CharField(u'Name', max_length=80)
    phone = models.CharField(u'Phone', max_length=15)
    address = models.CharField(u'Address', max_length=200)

    class Meta:
        verbose_name = 'Name'
        verbose_name_plural = 'Names'

    def __str__(self):
        return self.name