from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin



class CustomUserAdmin(UserAdmin):
    # model = User
    list_display = ('email', 'is_staff', 'is_active','is_superuser')
    list_filter = ('email', 'is_staff', 'is_active','is_superuser')
    fieldsets = (
        (None, {'fields': ('email', 'password','phone','birthdate','organization','groups')}),
        ('Permissions', {'fields': ('is_staff', 'is_active','is_superuser')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active','is_superuser','phone','birthdate','organization')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
# Register your models here.
admin.site.register(User,CustomUserAdmin)