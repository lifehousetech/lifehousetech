# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import Group
from rest_framework.permissions import (DjangoModelPermissions,BasePermission)


# PERMISOS
class CustomDjangoModelPermissions(DjangoModelPermissions):
    def __init__(self):
        self.perms_map['GET'] = ['%(app_label)s.view_%(model_name)s']

def is_in_group(user, group_name):
  try:
    return Group.objects.get(name=group_name).user_set.filter(id=user.id).exists()
  except Group.DoesNotExist:
    return False

class HasGroupPermission(BasePermission):
  def has_permission(self, request, view):
    required_groups = view.permission_groups.get(view.action)
    if required_groups == None:
    	return False
    elif '_Public' in required_groups:
        return True
    else:
        return any([is_in_group(request.user, group_name) for group_name in required_groups])