from rest_framework import serializers
from .models import User,Organization
from django.contrib.auth.models import Group,Permission 
from applications.organization.serializers import ListOrganizationSerializer

class ListPermissionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = ('name',)


class ListGroupSerializer(serializers.ModelSerializer):
    permissions = ListPermissionsSerializer(many=True)
    class Meta:
        model = Group
        fields = (
            'id',
            'name',
            'permissions',
        )

class ListUserSerializer(serializers.ModelSerializer):
    # groups = ListGroupSerializer(many=True)
    organization = ListOrganizationSerializer(required=False)

    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'phone',
            'email',
            'birthdate',
            'organization',
            # 'groups'
        )

class ListInfoSerializer (serializers.ModelSerializer):
    organization = ListOrganizationSerializer(required=False)
    client_ip = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'birthdate',
            'organization',
            'client_ip',
        )

    def get_client_ip(self,obj):
        request = self.context['request']
        if 'HTTP_X_FORWARDED_FOR' in request.META:
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            ip = x_forwarded_for.split(',')[0] 
        else:
            ip = request.META.get('REMOTE_ADDR') 
        return ip