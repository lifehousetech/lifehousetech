from django.shortcuts import render
# Import Rest Framework Generics
from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    GenericAPIView,
    RetrieveAPIView,
    RetrieveUpdateDestroyAPIView
)
from rest_framework.viewsets import ModelViewSet,GenericViewSet
from .serializers import ListUserSerializer,ListGroupSerializer,ListInfoSerializer
from applications.organization.serializers import ListOrganizationSerializer
from .models import User
from django.contrib.auth.models import Group
from .permissions import CustomDjangoModelPermissions,HasGroupPermission
from rest_framework.permissions import DjangoModelPermissions
from rest_framework import filters

# Create your views here.
"""USER"""
class ListCreateUserApiView(ListCreateAPIView):
    serializer_class = ListUserSerializer
    queryset = User.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['name','email','phone']
    permission_classes = (CustomDjangoModelPermissions,)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class UpdateRetrieveUserAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ListUserSerializer
    queryset = User.objects.all()
    permission_classes = (DjangoModelPermissions, )

    # permission_classes = [HasGroupPermission]
    # permission_groups = {
    #     'destroy': ['Administrator'],
    #     'partial_update': ['Administrator'],
    #     'retrieve': ['Administrator','Viewer'],
    # }
"""GROUP"""
class ListGroupApiView(ListAPIView):
    serializer_class = ListGroupSerializer
    queryset = Group.objects.all()

"""INFO"""

class InfoAPIView(ListAPIView):
    serializer_class = ListInfoSerializer
    def get_queryset(self):
        user = self.request.user
        return User.objects.filter(email=user)