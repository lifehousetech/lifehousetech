from django.urls import path
from . import views

urlpatterns = [
    path('api/auth/groups/', views.ListGroupApiView.as_view()),
    path('api/users/', views.ListCreateUserApiView.as_view()),
    path('api/users/<pk>/', views.UpdateRetrieveUserAPIView.as_view()),
    path('api/info/', views.InfoAPIView.as_view()),
]