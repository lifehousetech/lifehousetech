from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser
from applications.organization.models import Organization
from .managers import UserManager

class User(AbstractUser):
    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    name = models.CharField('Name',max_length=60)
    phone = models.CharField('Phone',max_length=15)
    email = models.EmailField('Email',unique=True)
    organization = models.ForeignKey(Organization, on_delete=models.SET_NULL,blank=True,null=True)
    birthdate = models.DateField('Birthdate', auto_now=False, auto_now_add=False,blank=True,null=True)

    objects = UserManager()

    def __str__(self):
        return self.email