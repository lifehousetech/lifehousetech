from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'z*ug_uzl7dk!6o^=v8s2+0ir^*sez%8_+%t4o!m*yngyvg(7wm'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['MAKE_SURE_ALLOW_HOST_PROD']

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dbpruebatecnica',
        'USER': 'postgres',
        'PASSWORD': 'MAKE_SURE_PASS_PROD',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'
